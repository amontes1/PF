﻿namespace PFConteo
{
    partial class PFConteo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEE = new System.Windows.Forms.TextBox();
            this.txtSE = new System.Windows.Forms.TextBox();
            this.txtCE = new System.Windows.Forms.TextBox();
            this.txtALI = new System.Windows.Forms.TextBox();
            this.txtAIE = new System.Windows.Forms.TextBox();
            this.rdbSimple = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbComplejo = new System.Windows.Forms.RadioButton();
            this.rdbPromedio = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtResEE = new System.Windows.Forms.TextBox();
            this.txtResSE = new System.Windows.Forms.TextBox();
            this.txtResCE = new System.Windows.Forms.TextBox();
            this.txtResALI = new System.Windows.Forms.TextBox();
            this.txtResAIE = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtF1 = new System.Windows.Forms.TextBox();
            this.txtF2 = new System.Windows.Forms.TextBox();
            this.txtF3 = new System.Windows.Forms.TextBox();
            this.txtF4 = new System.Windows.Forms.TextBox();
            this.txtF5 = new System.Windows.Forms.TextBox();
            this.txtF6 = new System.Windows.Forms.TextBox();
            this.txtF7 = new System.Windows.Forms.TextBox();
            this.txtF8 = new System.Windows.Forms.TextBox();
            this.txtF9 = new System.Windows.Forms.TextBox();
            this.txtF10 = new System.Windows.Forms.TextBox();
            this.txtF11 = new System.Windows.Forms.TextBox();
            this.txtF12 = new System.Windows.Forms.TextBox();
            this.txtF13 = new System.Windows.Forms.TextBox();
            this.txtF14 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtFactor = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.txtPFA = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.txtHH = new System.Windows.Forms.TextBox();
            this.txtDias = new System.Windows.Forms.TextBox();
            this.txtSemanas = new System.Windows.Forms.TextBox();
            this.txtMeses = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtHDiarias = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtTotalLineas = new System.Windows.Forms.TextBox();
            this.txtLineasPF = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtDuracion = new System.Windows.Forms.TextBox();
            this.txtDesarrolladores = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtCosto = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtOtrosCostos = new System.Windows.Forms.TextBox();
            this.txtSueldo = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.btnEstimar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "Valor de dominio \r\nde información";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(288, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Conteo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(441, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Simple";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(536, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Promedio";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(649, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Complejo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(504, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Factor ponderado";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(33, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Entradas externas (EE)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(33, 193);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Salidas externas (SE)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(33, 231);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(181, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Consultas externas (CE)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(30, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(222, 20);
            this.label10.TabIndex = 9;
            this.label10.Text = "Archivos lógicos internos (ALI)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(30, 314);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(254, 20);
            this.label11.TabIndex = 10;
            this.label11.Text = "Archivos de interfaz externos (AIE)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(30, 375);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 20);
            this.label12.TabIndex = 11;
            this.label12.Text = "Conteo total";
            // 
            // txtEE
            // 
            this.txtEE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEE.Location = new System.Drawing.Point(294, 143);
            this.txtEE.Name = "txtEE";
            this.txtEE.Size = new System.Drawing.Size(61, 26);
            this.txtEE.TabIndex = 12;
            // 
            // txtSE
            // 
            this.txtSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSE.Location = new System.Drawing.Point(294, 186);
            this.txtSE.Name = "txtSE";
            this.txtSE.Size = new System.Drawing.Size(61, 26);
            this.txtSE.TabIndex = 13;
            // 
            // txtCE
            // 
            this.txtCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCE.Location = new System.Drawing.Point(294, 224);
            this.txtCE.Name = "txtCE";
            this.txtCE.Size = new System.Drawing.Size(61, 26);
            this.txtCE.TabIndex = 14;
            // 
            // txtALI
            // 
            this.txtALI.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtALI.Location = new System.Drawing.Point(294, 267);
            this.txtALI.Name = "txtALI";
            this.txtALI.Size = new System.Drawing.Size(61, 26);
            this.txtALI.TabIndex = 15;
            // 
            // txtAIE
            // 
            this.txtAIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAIE.Location = new System.Drawing.Point(294, 314);
            this.txtAIE.Name = "txtAIE";
            this.txtAIE.Size = new System.Drawing.Size(61, 26);
            this.txtAIE.TabIndex = 16;
            // 
            // rdbSimple
            // 
            this.rdbSimple.AutoSize = true;
            this.rdbSimple.Location = new System.Drawing.Point(18, 19);
            this.rdbSimple.Name = "rdbSimple";
            this.rdbSimple.Size = new System.Drawing.Size(14, 13);
            this.rdbSimple.TabIndex = 17;
            this.rdbSimple.TabStop = true;
            this.rdbSimple.UseVisualStyleBackColor = true;
            this.rdbSimple.CheckedChanged += new System.EventHandler(this.rdbSimple_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbComplejo);
            this.groupBox1.Controls.Add(this.rdbPromedio);
            this.groupBox1.Controls.Add(this.rdbSimple);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(445, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 43);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // rdbComplejo
            // 
            this.rdbComplejo.AutoSize = true;
            this.rdbComplejo.Location = new System.Drawing.Point(238, 19);
            this.rdbComplejo.Name = "rdbComplejo";
            this.rdbComplejo.Size = new System.Drawing.Size(14, 13);
            this.rdbComplejo.TabIndex = 19;
            this.rdbComplejo.TabStop = true;
            this.rdbComplejo.UseVisualStyleBackColor = true;
            this.rdbComplejo.CheckedChanged += new System.EventHandler(this.rdbComplejo_CheckedChanged);
            // 
            // rdbPromedio
            // 
            this.rdbPromedio.AutoSize = true;
            this.rdbPromedio.Location = new System.Drawing.Point(127, 19);
            this.rdbPromedio.Name = "rdbPromedio";
            this.rdbPromedio.Size = new System.Drawing.Size(14, 13);
            this.rdbPromedio.TabIndex = 18;
            this.rdbPromedio.TabStop = true;
            this.rdbPromedio.UseVisualStyleBackColor = true;
            this.rdbPromedio.CheckedChanged += new System.EventHandler(this.rdbPromedio_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(388, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 20);
            this.label13.TabIndex = 19;
            this.label13.Text = "X";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(388, 189);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 20);
            this.label14.TabIndex = 20;
            this.label14.Text = "X";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(388, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 20);
            this.label15.TabIndex = 21;
            this.label15.Text = "X";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(388, 270);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 20);
            this.label16.TabIndex = 22;
            this.label16.Text = "X";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(388, 317);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 20);
            this.label17.TabIndex = 23;
            this.label17.Text = "X";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(459, 146);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(18, 20);
            this.label18.TabIndex = 24;
            this.label18.Text = "3";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(568, 146);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(18, 20);
            this.label19.TabIndex = 25;
            this.label19.Text = "4";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(684, 147);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 20);
            this.label20.TabIndex = 26;
            this.label20.Text = "6";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(684, 190);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(18, 20);
            this.label21.TabIndex = 29;
            this.label21.Text = "7";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(568, 189);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(18, 20);
            this.label22.TabIndex = 28;
            this.label22.Text = "5";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(459, 189);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(18, 20);
            this.label23.TabIndex = 27;
            this.label23.Text = "4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(684, 228);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(18, 20);
            this.label24.TabIndex = 32;
            this.label24.Text = "6";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(568, 227);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(18, 20);
            this.label25.TabIndex = 31;
            this.label25.Text = "4";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(459, 227);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(18, 20);
            this.label26.TabIndex = 30;
            this.label26.Text = "3";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(678, 271);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 20);
            this.label27.TabIndex = 35;
            this.label27.Text = "15";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(562, 270);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(27, 20);
            this.label28.TabIndex = 34;
            this.label28.Text = "10";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(459, 270);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(18, 20);
            this.label29.TabIndex = 33;
            this.label29.Text = "7";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(678, 317);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 20);
            this.label30.TabIndex = 38;
            this.label30.Text = "10";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(568, 317);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(18, 20);
            this.label31.TabIndex = 37;
            this.label31.Text = "7";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(459, 317);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(18, 20);
            this.label32.TabIndex = 36;
            this.label32.Text = "5";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(737, 147);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(18, 20);
            this.label33.TabIndex = 39;
            this.label33.Text = "=";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(737, 191);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(18, 20);
            this.label34.TabIndex = 40;
            this.label34.Text = "=";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(737, 228);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(18, 20);
            this.label35.TabIndex = 41;
            this.label35.Text = "=";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(737, 272);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(18, 20);
            this.label36.TabIndex = 42;
            this.label36.Text = "=";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(737, 315);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(18, 20);
            this.label37.TabIndex = 43;
            this.label37.Text = "=";
            // 
            // txtResEE
            // 
            this.txtResEE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResEE.Location = new System.Drawing.Point(787, 144);
            this.txtResEE.Name = "txtResEE";
            this.txtResEE.ReadOnly = true;
            this.txtResEE.Size = new System.Drawing.Size(68, 26);
            this.txtResEE.TabIndex = 44;
            // 
            // txtResSE
            // 
            this.txtResSE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResSE.Location = new System.Drawing.Point(787, 187);
            this.txtResSE.Name = "txtResSE";
            this.txtResSE.ReadOnly = true;
            this.txtResSE.Size = new System.Drawing.Size(68, 26);
            this.txtResSE.TabIndex = 45;
            // 
            // txtResCE
            // 
            this.txtResCE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResCE.Location = new System.Drawing.Point(787, 225);
            this.txtResCE.Name = "txtResCE";
            this.txtResCE.ReadOnly = true;
            this.txtResCE.Size = new System.Drawing.Size(68, 26);
            this.txtResCE.TabIndex = 46;
            // 
            // txtResALI
            // 
            this.txtResALI.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResALI.Location = new System.Drawing.Point(787, 268);
            this.txtResALI.Name = "txtResALI";
            this.txtResALI.ReadOnly = true;
            this.txtResALI.Size = new System.Drawing.Size(68, 26);
            this.txtResALI.TabIndex = 47;
            // 
            // txtResAIE
            // 
            this.txtResAIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResAIE.Location = new System.Drawing.Point(787, 311);
            this.txtResAIE.Name = "txtResAIE";
            this.txtResAIE.ReadOnly = true;
            this.txtResAIE.Size = new System.Drawing.Size(68, 26);
            this.txtResAIE.TabIndex = 48;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(787, 371);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(68, 26);
            this.txtTotal.TabIndex = 49;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(132, 371);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(648, 20);
            this.label38.TabIndex = 50;
            this.label38.Text = "_______________________________________________________________________";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(53, 427);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(124, 20);
            this.label39.TabIndex = 51;
            this.label39.Text = "Factor de ajuste";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(292, 427);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(63, 20);
            this.label40.TabIndex = 52;
            this.label40.Text = "Puntaje";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(29, 474);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(178, 20);
            this.label41.TabIndex = 53;
            this.label41.Text = "Comunicación de Datos";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(29, 514);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(194, 20);
            this.label42.TabIndex = 54;
            this.label42.Text = "Procesamiento Distribuido";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(30, 550);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(190, 20);
            this.label43.TabIndex = 55;
            this.label43.Text = "Objetivos de Rendimiento";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(29, 588);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(234, 20);
            this.label44.TabIndex = 56;
            this.label44.Text = "Configuración del Equipamiento";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(30, 624);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(169, 20);
            this.label45.TabIndex = 57;
            this.label45.Text = "Tasa de transacciones";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(30, 662);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(200, 20);
            this.label46.TabIndex = 58;
            this.label46.Text = "Entrada de Datos en Línea";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(29, 701);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(175, 20);
            this.label47.TabIndex = 59;
            this.label47.Text = "Interfase con el usuario";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(29, 735);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(185, 20);
            this.label48.TabIndex = 60;
            this.label48.Text = "Actualizaciones en Línea";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(30, 773);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(185, 20);
            this.label49.TabIndex = 61;
            this.label49.Text = "Procesamiento Complejo";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(30, 810);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(180, 20);
            this.label50.TabIndex = 62;
            this.label50.Text = "Reusabilidad del Código";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(30, 849);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(211, 20);
            this.label51.TabIndex = 63;
            this.label51.Text = "Facilidad de Implementación";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(29, 888);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(171, 20);
            this.label52.TabIndex = 64;
            this.label52.Text = "Facilidad de Operación";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(29, 929);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(169, 20);
            this.label53.TabIndex = 65;
            this.label53.Text = "Instalaciones Múltiples";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(30, 970);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(160, 20);
            this.label54.TabIndex = 66;
            this.label54.Text = "Facilidad de Cambios";
            // 
            // txtF1
            // 
            this.txtF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF1.Location = new System.Drawing.Point(296, 468);
            this.txtF1.Name = "txtF1";
            this.txtF1.Size = new System.Drawing.Size(51, 26);
            this.txtF1.TabIndex = 67;
            // 
            // txtF2
            // 
            this.txtF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF2.Location = new System.Drawing.Point(296, 508);
            this.txtF2.Name = "txtF2";
            this.txtF2.Size = new System.Drawing.Size(51, 26);
            this.txtF2.TabIndex = 68;
            // 
            // txtF3
            // 
            this.txtF3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF3.Location = new System.Drawing.Point(296, 547);
            this.txtF3.Name = "txtF3";
            this.txtF3.Size = new System.Drawing.Size(51, 26);
            this.txtF3.TabIndex = 69;
            // 
            // txtF4
            // 
            this.txtF4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF4.Location = new System.Drawing.Point(296, 585);
            this.txtF4.Name = "txtF4";
            this.txtF4.Size = new System.Drawing.Size(51, 26);
            this.txtF4.TabIndex = 70;
            // 
            // txtF5
            // 
            this.txtF5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF5.Location = new System.Drawing.Point(296, 621);
            this.txtF5.Name = "txtF5";
            this.txtF5.Size = new System.Drawing.Size(51, 26);
            this.txtF5.TabIndex = 71;
            // 
            // txtF6
            // 
            this.txtF6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF6.Location = new System.Drawing.Point(296, 656);
            this.txtF6.Name = "txtF6";
            this.txtF6.Size = new System.Drawing.Size(51, 26);
            this.txtF6.TabIndex = 72;
            // 
            // txtF7
            // 
            this.txtF7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF7.Location = new System.Drawing.Point(296, 695);
            this.txtF7.Name = "txtF7";
            this.txtF7.Size = new System.Drawing.Size(51, 26);
            this.txtF7.TabIndex = 73;
            // 
            // txtF8
            // 
            this.txtF8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF8.Location = new System.Drawing.Point(296, 732);
            this.txtF8.Name = "txtF8";
            this.txtF8.Size = new System.Drawing.Size(51, 26);
            this.txtF8.TabIndex = 74;
            // 
            // txtF9
            // 
            this.txtF9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF9.Location = new System.Drawing.Point(296, 770);
            this.txtF9.Name = "txtF9";
            this.txtF9.Size = new System.Drawing.Size(51, 26);
            this.txtF9.TabIndex = 75;
            // 
            // txtF10
            // 
            this.txtF10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF10.Location = new System.Drawing.Point(296, 807);
            this.txtF10.Name = "txtF10";
            this.txtF10.Size = new System.Drawing.Size(51, 26);
            this.txtF10.TabIndex = 76;
            // 
            // txtF11
            // 
            this.txtF11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF11.Location = new System.Drawing.Point(296, 846);
            this.txtF11.Name = "txtF11";
            this.txtF11.Size = new System.Drawing.Size(51, 26);
            this.txtF11.TabIndex = 77;
            // 
            // txtF12
            // 
            this.txtF12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF12.Location = new System.Drawing.Point(296, 885);
            this.txtF12.Name = "txtF12";
            this.txtF12.Size = new System.Drawing.Size(51, 26);
            this.txtF12.TabIndex = 78;
            // 
            // txtF13
            // 
            this.txtF13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF13.Location = new System.Drawing.Point(296, 926);
            this.txtF13.Name = "txtF13";
            this.txtF13.Size = new System.Drawing.Size(51, 26);
            this.txtF13.TabIndex = 79;
            // 
            // txtF14
            // 
            this.txtF14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtF14.Location = new System.Drawing.Point(296, 964);
            this.txtF14.Name = "txtF14";
            this.txtF14.Size = new System.Drawing.Size(51, 26);
            this.txtF14.TabIndex = 80;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(30, 1007);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(124, 20);
            this.label55.TabIndex = 81;
            this.label55.Text = "Factor de ajuste";
            // 
            // txtFactor
            // 
            this.txtFactor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactor.Location = new System.Drawing.Point(296, 1001);
            this.txtFactor.Name = "txtFactor";
            this.txtFactor.ReadOnly = true;
            this.txtFactor.Size = new System.Drawing.Size(51, 26);
            this.txtFactor.TabIndex = 82;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(459, 427);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(53, 20);
            this.label56.TabIndex = 83;
            this.label56.Text = "PFA =";
            // 
            // txtPFA
            // 
            this.txtPFA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPFA.Location = new System.Drawing.Point(518, 424);
            this.txtPFA.Name = "txtPFA";
            this.txtPFA.ReadOnly = true;
            this.txtPFA.Size = new System.Drawing.Size(51, 26);
            this.txtPFA.TabIndex = 84;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(59, 34);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(132, 20);
            this.label57.TabIndex = 87;
            this.label57.Text = "Horas - hombre =";
            // 
            // txtHH
            // 
            this.txtHH.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHH.Location = new System.Drawing.Point(193, 31);
            this.txtHH.Name = "txtHH";
            this.txtHH.ReadOnly = true;
            this.txtHH.Size = new System.Drawing.Size(51, 26);
            this.txtHH.TabIndex = 88;
            // 
            // txtDias
            // 
            this.txtDias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDias.Location = new System.Drawing.Point(193, 63);
            this.txtDias.Name = "txtDias";
            this.txtDias.ReadOnly = true;
            this.txtDias.Size = new System.Drawing.Size(51, 26);
            this.txtDias.TabIndex = 89;
            // 
            // txtSemanas
            // 
            this.txtSemanas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSemanas.Location = new System.Drawing.Point(193, 95);
            this.txtSemanas.Name = "txtSemanas";
            this.txtSemanas.ReadOnly = true;
            this.txtSemanas.Size = new System.Drawing.Size(51, 26);
            this.txtSemanas.TabIndex = 90;
            // 
            // txtMeses
            // 
            this.txtMeses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeses.Location = new System.Drawing.Point(193, 127);
            this.txtMeses.Name = "txtMeses";
            this.txtMeses.ReadOnly = true;
            this.txtMeses.Size = new System.Drawing.Size(51, 26);
            this.txtMeses.TabIndex = 91;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(250, 69);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(38, 20);
            this.label58.TabIndex = 92;
            this.label58.Text = "días";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(250, 101);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(74, 20);
            this.label59.TabIndex = 93;
            this.label59.Text = "semanas";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(250, 134);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 20);
            this.label60.TabIndex = 94;
            this.label60.Text = "meses";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(10, 66);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(102, 20);
            this.label61.TabIndex = 95;
            this.label61.Text = "Horas diarias";
            // 
            // txtHDiarias
            // 
            this.txtHDiarias.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHDiarias.Location = new System.Drawing.Point(118, 63);
            this.txtHDiarias.Name = "txtHDiarias";
            this.txtHDiarias.Size = new System.Drawing.Size(51, 26);
            this.txtHDiarias.TabIndex = 96;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label57);
            this.groupBox2.Controls.Add(this.txtHDiarias);
            this.groupBox2.Controls.Add(this.txtHH);
            this.groupBox2.Controls.Add(this.label61);
            this.groupBox2.Controls.Add(this.txtDias);
            this.groupBox2.Controls.Add(this.label60);
            this.groupBox2.Controls.Add(this.txtSemanas);
            this.groupBox2.Controls.Add(this.label59);
            this.groupBox2.Controls.Add(this.txtMeses);
            this.groupBox2.Controls.Add(this.label58);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(463, 479);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(335, 168);
            this.groupBox2.TabIndex = 97;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Horas - hombre";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtTotalLineas);
            this.groupBox3.Controls.Add(this.txtLineasPF);
            this.groupBox3.Controls.Add(this.label63);
            this.groupBox3.Controls.Add(this.label62);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(463, 666);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(335, 109);
            this.groupBox3.TabIndex = 98;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Líneas de código";
            // 
            // txtTotalLineas
            // 
            this.txtTotalLineas.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalLineas.Location = new System.Drawing.Point(196, 69);
            this.txtTotalLineas.Name = "txtTotalLineas";
            this.txtTotalLineas.ReadOnly = true;
            this.txtTotalLineas.Size = new System.Drawing.Size(51, 26);
            this.txtTotalLineas.TabIndex = 97;
            // 
            // txtLineasPF
            // 
            this.txtLineasPF.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLineasPF.Location = new System.Drawing.Point(196, 31);
            this.txtLineasPF.Name = "txtLineasPF";
            this.txtLineasPF.Size = new System.Drawing.Size(51, 26);
            this.txtLineasPF.TabIndex = 97;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(10, 75);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(184, 20);
            this.label63.TabIndex = 100;
            this.label63.Text = "Total de líneas de código";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(10, 37);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(180, 20);
            this.label62.TabIndex = 99;
            this.label62.Text = "Líneas de código por PF";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label69);
            this.groupBox4.Controls.Add(this.txtDuracion);
            this.groupBox4.Controls.Add(this.txtDesarrolladores);
            this.groupBox4.Controls.Add(this.label64);
            this.groupBox4.Controls.Add(this.label65);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(463, 796);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(335, 109);
            this.groupBox4.TabIndex = 101;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Duración del proyecto";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(253, 72);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(56, 20);
            this.label69.TabIndex = 101;
            this.label69.Text = "meses";
            // 
            // txtDuracion
            // 
            this.txtDuracion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDuracion.Location = new System.Drawing.Point(196, 69);
            this.txtDuracion.Name = "txtDuracion";
            this.txtDuracion.ReadOnly = true;
            this.txtDuracion.Size = new System.Drawing.Size(51, 26);
            this.txtDuracion.TabIndex = 97;
            // 
            // txtDesarrolladores
            // 
            this.txtDesarrolladores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDesarrolladores.Location = new System.Drawing.Point(196, 31);
            this.txtDesarrolladores.Name = "txtDesarrolladores";
            this.txtDesarrolladores.Size = new System.Drawing.Size(51, 26);
            this.txtDesarrolladores.TabIndex = 97;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(10, 75);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(73, 20);
            this.label64.TabIndex = 100;
            this.label64.Text = "Duración";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(10, 37);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(121, 20);
            this.label65.TabIndex = 99;
            this.label65.Text = "Desarrolladores";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtCosto);
            this.groupBox5.Controls.Add(this.label68);
            this.groupBox5.Controls.Add(this.txtOtrosCostos);
            this.groupBox5.Controls.Add(this.txtSueldo);
            this.groupBox5.Controls.Add(this.label66);
            this.groupBox5.Controls.Add(this.label67);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(847, 479);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(399, 168);
            this.groupBox5.TabIndex = 101;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Costo del proyecto";
            // 
            // txtCosto
            // 
            this.txtCosto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCosto.Location = new System.Drawing.Point(96, 118);
            this.txtCosto.Name = "txtCosto";
            this.txtCosto.ReadOnly = true;
            this.txtCosto.Size = new System.Drawing.Size(124, 26);
            this.txtCosto.TabIndex = 101;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(10, 121);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(80, 20);
            this.label68.TabIndex = 101;
            this.label68.Text = "Costo     $";
            // 
            // txtOtrosCostos
            // 
            this.txtOtrosCostos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOtrosCostos.Location = new System.Drawing.Point(252, 69);
            this.txtOtrosCostos.Name = "txtOtrosCostos";
            this.txtOtrosCostos.Size = new System.Drawing.Size(131, 26);
            this.txtOtrosCostos.TabIndex = 97;
            // 
            // txtSueldo
            // 
            this.txtSueldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSueldo.Location = new System.Drawing.Point(252, 31);
            this.txtSueldo.Name = "txtSueldo";
            this.txtSueldo.Size = new System.Drawing.Size(131, 26);
            this.txtSueldo.TabIndex = 97;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(10, 75);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(181, 20);
            this.label66.TabIndex = 100;
            this.label66.Text = "Otro costos del proyecto";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(10, 37);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(236, 20);
            this.label67.TabIndex = 99;
            this.label67.Text = "Sueldo mensual desarrolladores";
            // 
            // btnEstimar
            // 
            this.btnEstimar.AutoSize = true;
            this.btnEstimar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstimar.Location = new System.Drawing.Point(754, 420);
            this.btnEstimar.Name = "btnEstimar";
            this.btnEstimar.Size = new System.Drawing.Size(139, 30);
            this.btnEstimar.TabIndex = 102;
            this.btnEstimar.Text = "Estimar esfuerzo";
            this.btnEstimar.UseVisualStyleBackColor = true;
            this.btnEstimar.Click += new System.EventHandler(this.btnEstimar_Click);
            // 
            // PFConteo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1264, 1037);
            this.Controls.Add(this.btnEstimar);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.txtPFA);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.txtFactor);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.txtF14);
            this.Controls.Add(this.txtF13);
            this.Controls.Add(this.txtF12);
            this.Controls.Add(this.txtF11);
            this.Controls.Add(this.txtF10);
            this.Controls.Add(this.txtF9);
            this.Controls.Add(this.txtF8);
            this.Controls.Add(this.txtF7);
            this.Controls.Add(this.txtF6);
            this.Controls.Add(this.txtF5);
            this.Controls.Add(this.txtF4);
            this.Controls.Add(this.txtF3);
            this.Controls.Add(this.txtF2);
            this.Controls.Add(this.txtF1);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtResAIE);
            this.Controls.Add(this.txtResALI);
            this.Controls.Add(this.txtResCE);
            this.Controls.Add(this.txtResSE);
            this.Controls.Add(this.txtResEE);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtAIE);
            this.Controls.Add(this.txtALI);
            this.Controls.Add(this.txtCE);
            this.Controls.Add(this.txtSE);
            this.Controls.Add(this.txtEE);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "PFConteo";
            this.Text = "PFConteo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtEE;
        private System.Windows.Forms.TextBox txtSE;
        private System.Windows.Forms.TextBox txtCE;
        private System.Windows.Forms.TextBox txtALI;
        private System.Windows.Forms.TextBox txtAIE;
        private System.Windows.Forms.RadioButton rdbSimple;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbComplejo;
        private System.Windows.Forms.RadioButton rdbPromedio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtResEE;
        private System.Windows.Forms.TextBox txtResSE;
        private System.Windows.Forms.TextBox txtResCE;
        private System.Windows.Forms.TextBox txtResALI;
        private System.Windows.Forms.TextBox txtResAIE;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtF1;
        private System.Windows.Forms.TextBox txtF2;
        private System.Windows.Forms.TextBox txtF3;
        private System.Windows.Forms.TextBox txtF4;
        private System.Windows.Forms.TextBox txtF5;
        private System.Windows.Forms.TextBox txtF6;
        private System.Windows.Forms.TextBox txtF7;
        private System.Windows.Forms.TextBox txtF8;
        private System.Windows.Forms.TextBox txtF9;
        private System.Windows.Forms.TextBox txtF10;
        private System.Windows.Forms.TextBox txtF11;
        private System.Windows.Forms.TextBox txtF12;
        private System.Windows.Forms.TextBox txtF13;
        private System.Windows.Forms.TextBox txtF14;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtFactor;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox txtPFA;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtHH;
        private System.Windows.Forms.TextBox txtDias;
        private System.Windows.Forms.TextBox txtSemanas;
        private System.Windows.Forms.TextBox txtMeses;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox txtHDiarias;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtTotalLineas;
        private System.Windows.Forms.TextBox txtLineasPF;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtDuracion;
        private System.Windows.Forms.TextBox txtDesarrolladores;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtCosto;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtOtrosCostos;
        private System.Windows.Forms.TextBox txtSueldo;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button btnEstimar;
    }
}

