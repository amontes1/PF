﻿using System;
using System.Windows.Forms;

namespace PFConteo
{
    public partial class PFConteo : Form
    {
        private int ee, se, ce, ali, aie = 0;
        private int f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14;

        private void btnEstimar_Click(object sender, EventArgs e)
        {
            int pfsa = Convert.ToInt32(txtTotal.Text);
            int factor = factor_de_ajuste();
            int pfajustado = pfa();
            double horas_hombre = pfsa * 8;
            double dias = horas_hombre / Convert.ToInt32(txtHDiarias.Text);
            double semanas = dias / 5;
            double meses = semanas / 4;
            int total_lineas_codigo = pfsa * Convert.ToInt32(txtLineasPF.Text);
            double duracion = meses / Convert.ToInt32(txtDesarrolladores.Text);
            double costo = (Convert.ToInt32(txtDesarrolladores.Text) * Convert.ToInt32(txtSueldo.Text) * duracion) + Convert.ToInt32(txtOtrosCostos.Text);
            
            txtPFA.Text = pfajustado.ToString();
            txtHH.Text = horas_hombre.ToString();
            txtDias.Text = dias.ToString();
            txtSemanas.Text = semanas.ToString();
            txtMeses.Text = meses.ToString();
            txtTotalLineas.Text = total_lineas_codigo.ToString();
            txtDuracion.Text = duracion.ToString();
            txtCosto.Text = costo.ToString();
        }

        public PFConteo()
        {
            InitializeComponent();
        }

        private void rdbSimple_CheckedChanged(object sender, EventArgs e)
        {
            simple();

            mostrar_resultados();
        }

        private void rdbPromedio_CheckedChanged(object sender, EventArgs e)
        {
            promedio();

            mostrar_resultados();
        }

        private void rdbComplejo_CheckedChanged(object sender, EventArgs e)
        {
            complejo();

            mostrar_resultados();
        }

        private int pfa()
        {
            int pfa = (int)Math.Round(Convert.ToInt32(txtTotal.Text) * (0.65 + (0.01 * Convert.ToInt32(txtFactor.Text))));
           
            return pfa;
        }

        private int factor_de_ajuste()
        {
            int factor = 0;

            f1 = Convert.ToInt32(txtF1.Text);
            f2 = Convert.ToInt32(txtF2.Text);
            f3 = Convert.ToInt32(txtF3.Text);
            f4 = Convert.ToInt32(txtF4.Text);
            f5 = Convert.ToInt32(txtF5.Text);
            f6 = Convert.ToInt32(txtF6.Text);
            f7 = Convert.ToInt32(txtF7.Text);
            f8 = Convert.ToInt32(txtF8.Text);
            f9 = Convert.ToInt32(txtF9.Text);
            f10 = Convert.ToInt32(txtF10.Text);
            f11 = Convert.ToInt32(txtF11.Text);
            f12 = Convert.ToInt32(txtF12.Text);
            f13 = Convert.ToInt32(txtF13.Text);
            f14 = Convert.ToInt32(txtF14.Text);

            factor = f1 + f2 + f3 + f4 + f5 + f6 + f7 + f8 + f9 + f10 + f11 + f12 + f13 + f14;
            txtFactor.Text = factor.ToString();

            return factor;
        }

        private void simple()
        {
            ee = Convert.ToInt32(txtEE.Text) * 3;
            se = Convert.ToInt32(txtSE.Text) * 4;
            ce = Convert.ToInt32(txtCE.Text) * 3;
            ali = Convert.ToInt32(txtALI.Text) * 7;
            aie = Convert.ToInt32(txtAIE.Text) * 5;
        }

        private void promedio()
        {
            ee = Convert.ToInt32(txtEE.Text) * 4;
            se = Convert.ToInt32(txtSE.Text) * 5;
            ce = Convert.ToInt32(txtCE.Text) * 4;
            ali = Convert.ToInt32(txtALI.Text) * 10;
            aie = Convert.ToInt32(txtAIE.Text) * 7;
        }

        private void complejo()
        {
            ee = Convert.ToInt32(txtEE.Text) * 6;
            se = Convert.ToInt32(txtSE.Text) * 7;
            ce = Convert.ToInt32(txtCE.Text) * 6;
            ali = Convert.ToInt32(txtALI.Text) * 15;
            aie = Convert.ToInt32(txtAIE.Text) * 10;
        }

        private void mostrar_resultados()
        {
            int total = ee + se + ce + ali + aie;

            txtResEE.Text = ee.ToString();
            txtResSE.Text = se.ToString();
            txtResCE.Text = ce.ToString();
            txtResALI.Text = ali.ToString();
            txtResAIE.Text = aie.ToString();

            txtTotal.Text = total.ToString();
        }
    }
}
